#!/usr/bin/env python

from threading import Thread
import rospy
import math
from darwin_gazebo.darwin import Darwin
from geometry_msgs.msg import Twist
from std_msgs.msg import Int32


class Kick:
    """
    Class for making Darwin walk
    """
    def __init__(self,darwin):
        self.darwin=darwin

        self.kick_force = 1;

        
        self._sub_kick_force = rospy.Subscriber(darwin.ns + "kick_force", Int32, self._cb_kick_force, queue_size = 1) 
        self._sub_action = rospy.Subscriber(darwin.ns + "action", Int32, self._cb_action, queue_size = 1)

    def _cb_action(self, msg):
        # catching action
        rospy.loginfo("action")
        if msg.data == 1:
            self.step_first_right();
        else:
            self.step_first_left();

    def _cb_kick_force(self, msg):
        self.kick_force = msg.data

        
        
    def step_first_right(self):
        
        angles = {}
        angles["j_shoulder_r"] = -0.5;
        angles["j_high_arm_r"] = 1.3;
        angles["j_low_arm_r"] = 1.5;

        angles["j_shoulder_l"] = -angles["j_shoulder_r"] ;
        angles["j_high_arm_l"] = angles["j_high_arm_r"];
        angles["j_low_arm_l"] = -angles["j_low_arm_r"];

        angles["j_thigh1_r"] = 0.35;
        angles["j_thigh1_l"] = 0.35;
        angles["j_ankle2_r"] = 0.35;
        angles["j_ankle2_l"] = 0.35;

        angles["j_thigh2_r"] = -0.8;
        angles["j_tibia_r"] = 1.7;
        angles["j_tibia_l"] = -0.9;
        angles["j_ankle1_l"] = -0.3;
        angles["j_ankle1_r"] = 0.7;
       # angles["

        self.darwin.set_angles(angles);
        rospy.sleep(1);
        self.step_second_right();


    def step_second_right(self):
        speed_param = self.kick_force;

        n = 100
        i = 0
       
        angles = {}
        r = rospy.Rate(50)
        while i < n:
            persent = float(i) / n
    
            angles["j_thigh2_r"] = -0.8 - 0.2 * persent
            angles["j_tibia_r"] = 1.7 - 1.4 * persent
            angles["j_ankle1_r"] = (3.14/2 -(3.14 - angles["j_tibia_r"] - (3.14/2 + angles["j_thigh2_r"])))

            angles["j_shoulder_r"] = -0.5 + 0.5 * persent
            angles["j_low_arm_r"] = 1.5 + 0.3 * persent
    
            angles["j_shoulder_l"] = 0.5 + 0.5 * persent
            angles["j_low_arm_l"] =  -1.5 - 0.3 * persent

            self.darwin.set_angles(angles)
            i += speed_param
       
            r.sleep()
        rospy.sleep(0.5)
        self.ready_pos_right()

    def ready_pos_right(self):
        angles ={}
        
        speed_param = self.kick_force;
        n = 100
        i = 0
       
        angles = {}
        r = rospy.Rate(50);
        while i < n:
            persent = float(i) / n
    
            angles["j_thigh2_r"] = -1 + 0.3 * persent
            angles["j_tibia_r"] = 0.3 + 1.1 * persent
            angles["j_ankle1_r"] = (3.14/2 -(3.14 - angles["j_tibia_r"] - (3.14/2 + angles["j_thigh2_r"])))
            angles["j_thigh2_l"] = 0.6
            angles["j_tibia_l"] = -1
            angles["j_ankle1_l"]  = -(3.14/2 -(3.14 + angles["j_tibia_l"] - (3.14/2 - angles["j_thigh2_l"])))

            
            angles["j_shoulder_r"] = 0 - 0.5 * persent
            angles["j_low_arm_r"] = 1.8 - 0.3 * persent
    
            angles["j_shoulder_l"] = 1 - 0.5 * persent
            angles["j_low_arm_l"] = -1.8 + 0.3 * persent


            self.darwin.set_angles(angles)
            i += speed_param
       
            r.sleep()
        rospy.sleep(0.5)
        angles["j_thigh2_r"] = -0.6
        angles["j_tibia_r"] = 1
        angles["j_ankle1_r"] = (3.14/2 -(3.14 - angles["j_tibia_r"] - (3.14/2 + angles["j_thigh2_r"])))
        angles["j_thigh1_r"] = 0;
        angles["j_thigh1_l"] = 0;
        angles["j_ankle2_r"] = 0;
        angles["j_ankle2_l"] = 0;
	self.darwin.set_angles(angles)
    rospy.sleep(0.5)
        



    def step_first_left(self):
        
        angles = {}
        angles["j_shoulder_r"] = -0.5;
        angles["j_high_arm_r"] = 1.3;
        angles["j_low_arm_r"] = 1.5;

        angles["j_shoulder_l"] = -angles["j_shoulder_r"] ;
        angles["j_high_arm_l"] = angles["j_high_arm_r"];
        angles["j_low_arm_l"] = -angles["j_low_arm_r"];

        angles["j_thigh1_r"] = -0.35;
        angles["j_thigh1_l"] = -0.35;
        angles["j_ankle2_r"] = -0.35;
        angles["j_ankle2_l"] = -0.35;

        angles["j_thigh2_l"] = 0.8;
        angles["j_tibia_l"] = -1.7;
        angles["j_tibia_r"] = 0.9;
        angles["j_ankle1_r"] = 0.3;
        angles["j_ankle1_l"] = -0.7;
       # angles["

        self.darwin.set_angles(angles);
        rospy.sleep(1);
        self.step_second_left();


    def step_second_left(self):
        speed_param = self.kick_force;

        n = 100
        i = 0
       
        angles = {}
        r = rospy.Rate(50)
        while i < n:
            persent = float(i) / n
    
            angles["j_thigh2_l"] = 0.8 + 0.2 * persent
            angles["j_tibia_l"] = -1.7 + 1.4 * persent
            angles["j_ankle1_l"] = (3.14/2 -(3.14 - angles["j_tibia_l"] - (3.14/2 + angles["j_thigh2_l"])))

            angles["j_shoulder_l"] = 0.5 - 0.5 * persent
            angles["j_low_arm_l"] = -1.5 - 0.3 * persent
    
            angles["j_shoulder_r"] = -0.5 - 0.5 * persent
            angles["j_low_arm_r"] =  1.5 + 0.3 * persent

            self.darwin.set_angles(angles)
            i += speed_param
       
            r.sleep()
        rospy.sleep(0.5)
        self.ready_pos_left()

    def ready_pos_left(self):
        angles ={}
        
        speed_param = self.kick_force;
        n = 100
        i = 0
       
        angles = {}
        r = rospy.Rate(50);
        while i < n:
            persent = float(i) / n
    
            angles["j_thigh2_l"] = 1 - 0.3 * persent
            angles["j_tibia_l"] = -0.3 - 1.1 * persent
            angles["j_ankle1_l"] = -(3.14/2 -(3.14 + angles["j_tibia_l"] - (3.14/2 - angles["j_thigh2_l"])))
            angles["j_thigh2_r"] = -0.6
            angles["j_tibia_r"] = 1
            angles["j_ankle1_r"]  = (3.14/2 -(3.14 - angles["j_tibia_r"] - (3.14/2 + angles["j_thigh2_r"])))

            
            angles["j_shoulder_l"] = 0 + 0.5 * persent
            angles["j_low_arm_l"] = -1.8 + 0.3 * persent
    
            angles["j_shoulder_r"] = -1 + 0.5 * persent
            angles["j_low_arm_r"] = 1.8 - 0.3 * persent


            self.darwin.set_angles(angles)
            i += speed_param
       
            r.sleep()
        rospy.sleep(0.5)
        angles["j_thigh2_l"] = 0.6
        angles["j_tibia_l"] = -1
        angles["j_ankle1_l"] = -(3.14/2 -(3.14 + angles["j_tibia_l"] - (3.14/2 - angles["j_thigh2_l"])))
        angles["j_thigh1_r"] = 0;
        angles["j_thigh1_l"] = 0;
        angles["j_ankle2_r"] = 0;
        angles["j_ankle2_l"] = 0;
        self.darwin.set_angles(angles)
        rospy.sleep(0.5)
        


  

if __name__=="__main__":
    rospy.init_node("kick")
    rospy.sleep(1)
    
    rospy.loginfo("Instantiating Darwin Client")
    darwin=Darwin()
    rospy.loginfo("Instantiating Darwin Walker")
    kick=Kick(darwin)
 
    rospy.loginfo("Darwin Walker Ready")
    while not rospy.is_shutdown():
        rospy.sleep(1)
