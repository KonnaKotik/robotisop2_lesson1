
#include <robotis_op_ball_tracker/robotis_op_ball_tracker_node.h>

using namespace robotis_op;

#include <opencv2/opencv.hpp>

#include <iostream>
#include <stdlib.h> 

#include <stdlib.h>     /* srand, rand */
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include <math.h>
#include <sensor_msgs/image_encodings.h>

#include <image_transport/image_transport.h>
namespace robotis_op {


RobotisOPBallTrackingNode::RobotisOPBallTrackingNode(ros::NodeHandle nh)
    : nh_(nh)
{
    ros::NodeHandle nh_;

    joint_states_sub_ = nh_.subscribe("/darwin/joint_states", 100, &RobotisOPBallTrackingNode::jointStatesCb, this);
    image_sub_ = nh_.subscribe("/darwin/camera/image_raw", 1, &RobotisOPBallTrackingNode::imageCb, this);

    transformed_img_pub_ =nh_.advertise<sensor_msgs::Image>("/darwin/ball_tracker/image_transformed_raw", 100); // пока не знаю откуда 
    blob_img_pub_ =nh_.advertise<sensor_msgs::Image>("/darwin/ball_tracker/image_blob_raw", 100);// пока не знаю откуда 
    tilt_pub_ = nh_.advertise<std_msgs::Float64>("/darwin/j_tilt_position_controller/command", 100);
    pan_pub_ = nh_.advertise<std_msgs::Float64>("/darwin/j_pan_position_controller/command", 100);
    vel_pub_ = nh_.advertise<geometry_msgs::Twist>("/darwin/cmd_vel", 1);
    kick_force_pub_ = nh_.advertise<std_msgs::Int32>("/darwin/kick_force", 100);
    kick_action_pub_ = nh.advertise<std_msgs::Int32>("/darwin/action", 100);

    count_no_detection_ = 0;
    count_search_loop_ = 0;
    go_count=0;
    direction = 0;
    gone = false;
    detected = false;
    needWalk = true;

    m_KickRightAngle = -20.0*3.14/180;
    m_KickLeftAngle = 20.0 *3.14/180;
    m_KickCentreRightAbgle = -5.0*3.14/180;
    m_KickCentreLeftAbgle = 5.0*3.14/180;
/*
    dp_ = 1.0;
    minDist_ = 1.0;
    param1_ = 200;
    param2_ = 10;
    min_radius_ = 0.0;
    max_radius_ = 0.0;*/
}

RobotisOPBallTrackingNode::~RobotisOPBallTrackingNode()
{
}


void RobotisOPBallTrackingNode::jointStatesCb(const sensor_msgs::JointState& msg) // изначальное полоение головы
{
    pan_ = msg.position.at(10);
    tilt_ = msg.position.at(21);
}

void RobotisOPBallTrackingNode::dynamicReconfigureCb(robotis_op_ball_tracker::robotis_op_ball_trackerConfig &config, uint32_t level) // не разобрала ( скорее динамическое изменение изображение мяча)
{

    dp_ = config.dp;
    minDist_ = config.minDist;
    param1_ = config.param1;
    param2_ = config.param2;
    min_radius_ = config.minRadius;
    max_radius_ = config.maxRadius;
}


static double angle( cv::Point pt1, cv::Point pt2, cv::Point pt0 ) // изменне координат
{
    double dx1 = pt1.x - pt0.x;
    double dy1 = pt1.y - pt0.y;
    double dx2 = pt2.x - pt0.x;
    double dy2 = pt2.y - pt0.y;
    return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}

bool RobotisOPBallTrackingNode::findBallFunc(cv::Mat& mat_trans, cv::Mat& mat_blob, cv::Point& ball_position) { //ищет мяч


ROS_INFO("START findBallFunc");
    std::vector<cv::Mat> canales = std::vector<cv::Mat>();
    cv::split(mat_trans, canales);

    canales[0] =  2*canales[0]-canales[1]-canales[2];
    canales[1] = canales[2]-canales[2];
    canales[2] = canales[2]-canales[2];
    cv::merge(canales, mat_trans);

    cv::cvtColor(mat_trans,mat_trans, CV_RGB2GRAY);

    std::vector<cv::Vec3f> circles;

    ROS_INFO("%f %f %f %f %f %f",dp_, minDist_, param1_, param2_,min_radius_, max_radius_);

    cv::HoughCircles(mat_trans, circles, CV_HOUGH_GRADIENT, dp_, minDist_, param1_, param2_,min_radius_, max_radius_);//1,  mat_trans.rows/8, 200, 10, 0, 0 ); // функция для обнаружения мяча из OpenCV




    ROS_INFO("detected %i circles",(int)circles.size());
    bool ball_detected = false;
    float min_dist;
    if(circles.size() > 0)
        min_dist = squaredDistXY(previous_position_, circles[0]);
    int min_idx = 0;
    for( size_t i = 0; i < circles.size(); i++ )
    {
        ball_detected = true;
        cv::Point center((circles[i][0]), (circles[i][1]));
        ball_position = center;
        int radius = (circles[i][2]);
        // circle center

        cv::circle( mat_blob, center, 3, cv::Scalar(0,255,0), -1, 8, 0 );
        // circle outline

        cv::circle( mat_blob, center, radius, cv::Scalar(0,0,255), 3, 8, 0 );
        if(squaredDistXY(previous_position_, circles[0]) < min_dist)
        {
            min_idx = i;
            min_dist = squaredDistXY(previous_position_, circles[0]);
            ball_position = center;
        }
    }
    if(circles.size() > 0)
    {
        previous_position_ = circles[min_idx];
    }

    return ball_detected;
}


bool RobotisOPBallTrackingNode::findGoalFunc(cv::Mat& mat_trans, cv::Point& offset, cv::Point& width) {

    
    ROS_INFO("start detect goal");
    std::vector<std::vector<cv::Point> > contours; 
    std::vector<cv::Vec4i> hierarchy;
    cv::findContours(mat_trans,contours,hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

    std::vector<cv::Point> approx;

    int maxX = -1, minX = 5000, sr;
 	ROS_INFO("size contours %i", contours.size());
    for (int i = 0; i < contours.size(); i++)
    {
        cv::Point p1(-1,-1), p2(-1,5000), p3(5000, 5000), p4(5000,-1);
        cv::approxPolyDP(cv::Mat(contours[i]), approx, cv::arcLength(cv::Mat(contours[i]), true)*0.02, true);
        ROS_INFO("approx size %d", approx.size());
        if (approx.size() >= 6 && approx.size() <= 12)
        {
            ROS_INFO("image^^ %i %i", mat_trans.cols, mat_trans.rows);
            // ROS_INFO("I FIND %d %d kolvo %i", approx[0].x, approx[0].y, approx.size());
             for (int k = 0; k < approx.size(); k++){
                if(approx[k].x > maxX) maxX = approx[k].x;
                if(approx[k].x < minX) minX = approx[k].x;
                // ROS_INFO("%i  ==  %d,  %d", k, approx[k].x, approx[k].y);
             }
 
            sr = (maxX-minX)/2.0 + minX;
            //sr = mat_trans.cols / 2;
            int hi = 0;
            for (int k =0; k < approx.size(); k++) {
                if (approx[k].x < sr) {
                    if(approx[k].y > p1.y) p1 = approx[k];
                    if(approx[k].y < p2.y) p2 = approx[k];
                }
                else{
                    if(approx[k].y > p4.y) p4 = approx[k];
                    if(approx[k].y < p3.y) p3 = approx[k];
                }        
                if (mat_trans.size().height/2.0 >= approx[k].y) hi++;    
                 
            }

            bool allInit = (p1.x == -1 && p1.y == -1) || (p2.x == -1 && p2.y == 5000) || (p3.x == 5000 && p3.y == 5000) || (p4.x == 5000 && p4.y == -1);
            double x_cen = mat_trans.size().width/2.0;
            bool full = p1.x < x_cen && p4.x > x_cen;
            width.x = p3.x-p1.x;
            double angle1 = angle(p1, p3, p2);
            double angle2 = angle(p2, p4, p3);
            
            ROS_INFO("angles %f", std::fabs(angle1 + angle2) );
          //  ROS_INFO("angles %d gg", angle1);
            if(std::fabs(angle1 + angle2) < 0.4 && hi >= 4 && !allInit && full) {
                //goal_detected = true;

                
                ROS_INFO("I FIND");
                cv::Mat test = mat_trans;
                imwrite( "/home/arina/Документы/robotis_op2_image/GrayIFind_Image.jpg", test );
                cv::Point goal_position(sr, mat_trans.size().height/2.0);
                cv::Point image_center(mat_trans.size().width/2.0,mat_trans.size().height/2.0);
                offset = goal_position - image_center ;
                return true;
            }
        
        }
    
    }
    return false;
}

void RobotisOPBallTrackingNode::easyKick(int kickForceData, int direction) {
     ros::Duration(2).sleep();

    ROS_INFO("i kick");
     std_msgs::Int32 kickForce;
     kickForce.data = kickForceData;
     kick_force_pub_.publish(kickForce);

     std_msgs::Int32 kickBall;
     kickBall.data = direction;
     kick_action_pub_.publish(kickBall);

    ros::Duration(4).sleep();
            
}


void RobotisOPBallTrackingNode::walkWithBall(cv::Mat& mat_trans, cv::Mat& mat_blob) {
    ROS_INFO("I START WALK");

    cv::Point ball_position = cv::Point(-1, -1);

    bool ball_detected = findBallFunc(mat_trans, mat_blob, ball_position);

   
    if(ball_detected)
    {
        count_no_detection_ = 0;
        cv::Point image_center(mat_blob.size().width/2.0,mat_blob.size().height/2.0);
        cv::Point offset = ball_position - image_center ;
        ROS_INFO("ball pos  %i %i",ball_position.x, ball_position.y);
        ROS_INFO("ball pos offset %i %i",offset.x, offset.y);

            //head movement
        double tilt_scale_ = 0.001;
        double pan_scale_ = 0.001;
        std_msgs::Float64 angle_msg;
        angle_msg.data = tilt_-tilt_scale_*offset.y;
        tilt_pub_.publish(angle_msg);
        angle_msg.data = pan_-pan_scale_*offset.x;
        pan_pub_.publish(angle_msg);

    }
    else {
        std_msgs::Float64 angle_msg;
        angle_msg.data =  (sin(count_search_loop_*6.24/90))*0.5 - 0.5;
        tilt_pub_.publish(angle_msg);

        count_search_loop_++;
        return;
    }

    geometry_msgs::Twist vel;
    vel.angular.z = 0.0;
    
    if (tilt_ > -0.95) {
        vel.linear.x = 0.5;
        vel.linear.y = 0;
        vel_pub_.publish(vel);
        return;
    }
    else if (pan_ < m_KickRightAngle || (pan_ >+ 0 && pan_ < m_KickCentreLeftAbgle)) {
        vel.linear.x = 0;
        vel.linear.y = 0.5;
        vel_pub_.publish(vel);
        return;
    
    }
    else if (pan_ > m_KickLeftAngle || (pan_ < 0 && pan_ > m_KickCentreRightAbgle)) {
        vel.linear.x = 0;
        vel.linear.y = -0.5;
        vel_pub_.publish(vel);
        return;
    }
    else {
        vel.linear.x = 0;
        vel.linear.y = 0;
        vel_pub_.publish(vel);
        gone = false;
        detected = false;
    }
    
    
    if (pan_ > m_KickRightAngle && pan_ < m_KickLeftAngle && tilt_ < 0.9) {

        //direction -- 1 - right kick, 2 - left kick

        int kickForceData;
        if (needWalk) kickForceData = 3;
        else kickForceData = 30;

        if (pan_ < 0  ) {
            easyKick(kickForceData, 1);
        }
        else {
            easyKick(kickForceData, 2);
        }

        
        gone = false;
        detected = false;
        needWalk = true;
        direction = 0;

        double tilt_scale_ = 0.0;
        std_msgs::Float64 angle_msg;
        angle_msg.data = tilt_scale_;
        tilt_pub_.publish(angle_msg);
         ros::Duration(1).sleep();

    }

    //TODO do go to ball

    // else {
    //     geometry_msgs::Twist vel;
    // vel.angular.z = 0.0;
    // vel.linear.x = 0.3;
    // vel.linear.y = 0;
    // vel_pub_.publish(vel);
    //     if (pan_ < m_KickRightAngle) {

    //     }
    // }




/*

    if (go_count < 20){

    ROS_INFO("start walking");
    geometry_msgs::Twist vel;
    vel.angular.z = 0.01;
    vel.linear.x = 0.3;
    vel.linear.y = 0;
    vel_pub_.publish(vel);
    
    go_count++;
    }
    else {
        //head movement

        ROS_INFO("start tracking goal");
        cv::Point offset;
        cv::Point width(0,0);
        if(findGoalFunc(mat_trans, offset, width)) {
            ROS_INFO("i found and go to");
            if(width.x >= 235) {
                ROS_INFO("width: %i", width.x);
                geometry_msgs::Twist vel;
                vel.angular.z = 0;
                vel.linear.x = 0;
                vel.linear.y = 0;//l_scale_*joy->axes[axis_linear_y_];
                vel_pub_.publish(vel);
            }
            else {
            double pan_scale_ = 0.001;
            std_msgs::Float64 angle_msg;
            angle_msg.data = pan_-pan_scale_*offset.x;
            pan_pub_.publish(angle_msg);

            //go_count = 10;
        
            //walking
            double a_scale_ = 1;
            geometry_msgs::Twist vel;
            vel.angular.z = a_scale_*(angle_msg.data);
            vel.linear.x = std::max(0.3,0.6- std::abs(5.0*vel.angular.z));//l_scale_*joy->axes[axis_linear_x_];
            vel.linear.y = 0;//l_scale_*joy->axes[axis_linear_y_];
            vel_pub_.publish(vel);}
            }
        else {
            ROS_INFO("i try find");
            std_msgs::Float64 angle_msg;
               angle_msg.data =  (sin(count_search_loop_*6.24/270));
            pan_pub_.publish(angle_msg);
            count_search_loop_++;

                geometry_msgs::Twist vel;
    vel.angular.z = 0;
    vel.linear.x = 0.4;
    vel.linear.y = -0.01;
    vel_pub_.publish(vel);
        }
    }*/
    
}



void RobotisOPBallTrackingNode::findGoal(cv::Mat& mat_trans, cv::Mat& mat_blob){

    if(detected) {
        //Если уже повернулся к воротам	
	ROS_INFO("Goal");
        walkWithBall(mat_trans, mat_blob);
        
        return;
    }
    cv::inRange(mat_trans,cv::Scalar(170,170,170), cv::Scalar(190,190,190),mat_trans); // проверяет лежит ли размытое изображение в диапозоне данных цветов

    cv::Point offset;//точка смещения
    cv::Point width(0,0);//мэйби расстояние

    if(direction == 0) {
        //Если направления ещё нет, то он сначла ищет ворота
        std_msgs::Float64 angle_msg;
        angle_msg.data =  (sin(count_search_loop_*6.24/270));//!узнать почему именноо эти цифры!
        pan_pub_.publish(angle_msg);
        count_search_loop_++;
	ROS_INFO("find goal if direction == 0");

        if(findGoalFunc(mat_trans, offset, width)) { //передает
            imwrite( "/home/arina/Документы/robotis_op2_image/Gray_Image.jpg", mat_trans );
            if (pan_ >= 0) {
	direction = 1;
		//ROS_INFO("find goal if direction == 0");
}
            else direction = -1;

            count_search_loop_ = 0;
            angle_msg.data = 0;
            pan_pub_.publish(angle_msg);
            ros::Duration(1.5).sleep();
            
        }
        return;
    }
    ROS_INFO("direction %i", direction);

    
    bool goal_detected = findGoalFunc(mat_trans, offset, width);

    if(goal_detected && abs(offset.x) < 35){
        if (width.x > 238) needWalk = false;

        detected = true;
        
        geometry_msgs::Twist vel;
        vel.angular.z = 0;
        vel.linear.x = 0;
        vel.linear.y = 0;
        vel_pub_.publish(vel);

        count_search_loop_ = 0;

        ROS_INFO("offset: %d %d", offset.x, offset.y);
        return;
  
    }
    else
    {

        geometry_msgs::Twist vel;
        vel.angular.z = 0.34*direction;
        vel.linear.x = 0;
        vel.linear.y = -0.15*direction;
        vel_pub_.publish(vel);

        count_search_loop_++;
    }
}
    

//http://wiki.ros.org/cv_bridge/Tutorials/UsingCvBridgeToConvertBetweenROSImagesAndOpenCVImages
void RobotisOPBallTrackingNode::imageCb(const sensor_msgs::Image& msg)
{
    ROS_INFO("cb");

    ROS_INFO("%s", detected ? "True" : "False");

    ROS_INFO("%s gone", gone ? "True" : "False");

    //DETECT BALL
    cv_bridge::CvImagePtr image_trans, image_blob;
    image_trans = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::RGB8);
    image_blob = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::RGB8);

    cv::Mat& mat_blob = image_blob->image;
    cv::Mat& mat_trans = image_trans->image;
    cv::Mat test = image_trans -> image;
    imwrite( "/home/arina/Документы/robotis_op2_image/Test_Image.jpg", test );
    imshow( "Test", test );
    cv::GaussianBlur( mat_trans, mat_trans, cv::Size(9, 9), 2, 2 ); //размытие изораженеи с помощью фильтра Гаусса

    
    if(gone) {

        //Если дошли до мяча, то ищем ворота
        findGoal(mat_trans, mat_blob);//передаем размытое и обычное изображение
      ROS_INFO("Ищу ворота");
        return;
    }
    

    cv::Point ball_position = cv::Point(-1, -1);

    bool ball_detected = findBallFunc(mat_trans, mat_blob, ball_position);
    if(ball_detected)
    {
        count_no_detection_ = 0;
        cv::Point image_center(mat_blob.size().width/2.0,mat_blob.size().height/2.0);
        cv::Point offset = ball_position - image_center ;
        ROS_INFO("ball pos  %i %i",ball_position.x, ball_position.y);
        ROS_INFO("ball pos offset %i %i",offset.x, offset.y);

        if (tilt_ >= -0.9) {

            //head movement
            double tilt_scale_ = 0.001;
            double pan_scale_ = 0.001;
            std_msgs::Float64 angle_msg;
            angle_msg.data = tilt_-tilt_scale_*offset.y;
            tilt_pub_.publish(angle_msg);
            angle_msg.data = pan_-pan_scale_*offset.x;
            pan_pub_.publish(angle_msg);
    
            //walking
            double a_scale_ = 0.5;
            geometry_msgs::Twist vel;
            vel.angular.z = a_scale_*(angle_msg.data);
            vel.linear.x = std::max(0.0,0.8- std::abs(5.0*vel.angular.z));//l_scale_*joy->axes[axis_linear_x_];
            vel.linear.y = 0;//l_scale_*joy->axes[axis_linear_y_];
            vel_pub_.publish(vel);
        }
        else {

            ROS_INFO("I AM HERE");
            //Мы подошли, останавливаемся и возвращаемся в исходное
            //stop
            geometry_msgs::Twist vel;
            vel.angular.z = 0;
            vel.linear.x = 0;
            vel.linear.y = 0;
            vel_pub_.publish(vel);
          
            
            double tilt_scale_ = 0.0;
            double pan_scale_ = 0.0;
            std_msgs::Float64 angle_msg;
            angle_msg.data = tilt_scale_;
            tilt_pub_.publish(angle_msg);
            angle_msg.data = pan_scale_;
            pan_pub_.publish(angle_msg);

            gone = true;
        }
    }
    else if (count_no_detection_< 10)
    {
        count_no_detection_++;
        count_search_loop_ = 0;
    }
    else
    {
        //search loop
        std_msgs::Float64 angle_msg;
        angle_msg.data =  (sin(count_search_loop_*6.24/90))*0.5 - 0.5;
        tilt_pub_.publish(angle_msg);

        geometry_msgs::Twist vel;
        vel.angular.z = 0;
        vel.linear.x = 0;
        vel.linear.y = 0;
        vel_pub_.publish(vel);

        count_search_loop_++;
    }


}


}



int main(int argc, char **argv)
{

    ros::init(argc, argv, ROS_PACKAGE_NAME);

    ros::NodeHandle nh;
    double control_rate;
    nh.param("robotis_op_walking/control_rate", control_rate, 125.0);
    control_rate = 125.0;

    RobotisOPBallTrackingNode gazebo_walking_node(nh);

    ros::AsyncSpinner spinner(4);
    spinner.start();

    ros::Time last_time = ros::Time::now();
    ros::Rate rate(control_rate);

    dynamic_reconfigure::Server<robotis_op_ball_tracker::robotis_op_ball_trackerConfig> srv;
    dynamic_reconfigure::Server<robotis_op_ball_tracker::robotis_op_ball_trackerConfig>::CallbackType cb;
    cb = boost::bind(&RobotisOPBallTrackingNode::dynamicReconfigureCb, &gazebo_walking_node, _1, _2);
    srv.setCallback(cb);


    ROS_INFO("Ball tracking enabled");

    while (ros::ok())
    {
        rate.sleep();
        ros::Time current_time = ros::Time::now();
        ros::Duration elapsed_time = current_time - last_time;
        //gazebo_walking_node.Process();
        last_time = current_time;

    }

    return 0;
}

