#include <termios.h>
#include <signal.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/poll.h>


#include <boost/thread/thread.hpp>
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Bool.h>
#include <sensor_msgs/JointState.h>
#include <ros/package.h>

#include <iostream>
#include <fstream>

using namespace std;

ros::Publisher pub_;
ros::Publisher tilt_pub_;
ros::Publisher pan_pub_;

ros::Publisher action_pub_;
ros::Publisher enable_walking_pub_;

ros::Publisher j_low_arm_l;
ros::Publisher j_high_arm_l;
ros::Publisher j_shoulder_l;
ros::Publisher j_low_arm_r;
ros::Publisher j_high_arm_r;
ros::Publisher j_shoulder_r; 
/*
ros::Publisher j_thigh2_l;
ros::Publisher j_thigh2_r;
ros::Publisher j_tibia_l; 
ros::Publisher j_tibia_r; 
ros::Publisher j_pelvis_l; 
ros::Publisher j_pelvis_r; 
ros::Publisher j_ankle2_r; 
ros::Publisher j_ankle2_l;*/


int main(int argc, char **argv) {

    ros::init(argc, argv, "robotis_dance", ros::init_options::AnonymousName | ros::init_options::NoSigintHandler);

    std_msgs::Float64 angle_msg;



    ros::NodeHandle n_;

    //8 joins
    const int n_joins = 8;
    ros::Publisher command[n_joins] = {
            tilt_pub_ = n_.advertise<std_msgs::Float64>("/darwin/j_tilt_position_controller/command", 100),

            pan_pub_ = n_.advertise<std_msgs::Float64>("/darwin/j_pan_position_controller/command", 100),

            j_shoulder_l = n_.advertise<std_msgs::Float64>("/darwin/j_shoulder_l_position_controller/command",
            100),
            j_shoulder_r = n_.advertise<std_msgs::Float64>("/darwin/j_shoulder_r_position_controller/command",
            100),
            j_high_arm_l = n_.advertise<std_msgs::Float64>("/darwin/j_high_arm_l_position_controller/command",
            100),
            j_high_arm_r = n_.advertise<std_msgs::Float64>("/darwin/j_high_arm_r_position_controller/command",
            100),
            j_low_arm_l = n_.advertise<std_msgs::Float64>("/darwin/j_low_arm_l_position_controller/command",
            100),
            j_low_arm_r = n_.advertise<std_msgs::Float64>("/darwin/j_low_arm_r_position_controller/command",
            100)

    };


   	// так можно найти файл
      ifstream file((ros::package::getPath("robotis_dance") + "/data/" +  argv[1]), ios::in | ios::binary);

	

	
   
    

  
   
        
             /*  
		Пример отправления сообщения (координат) определенной ноде, 
		где command[s] - это одна из 8 нод
		    angle_msg.data - сообщение с координатами
		angle_msg.data = std::stof(k[i][s], &sz);
                command[s].publish(angle_msg);
                ros::spinOnce();
*/
         

    return 0;
}

