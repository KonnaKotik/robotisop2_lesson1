#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables
export LD_LIBRARY_PATH="/home/arina/catkin_ws/devel/lib:/opt/ros/melodic/lib"

# modified environment variables
export CMAKE_PREFIX_PATH="/home/arina/catkin_ws/src/robotis_dance/cmake-build-debug/devel:$CMAKE_PREFIX_PATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/arina/catkin_ws/src/robotis_dance/cmake-build-debug/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/arina/catkin_ws/src/robotis_dance:$ROS_PACKAGE_PATH"